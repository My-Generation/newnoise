package com.future.noise.dao;

import com.future.noise.model.Admin;
import org.apache.ibatis.annotations.Param;

public interface AdminDAO {
    //保存
    void save(Admin admin);
    //查询
    Admin queryByAdminNameAndPassword(@Param("adminName")String adminName,@Param("password")String password);
}
