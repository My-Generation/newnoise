package com.future.noise.controller;

import com.future.noise.model.Admin;
import com.future.noise.model.User;
import com.future.noise.service.AdminService;
import com.future.noise.service.UserService;
import com.future.noise.utils.UUIDUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @Autowired
    private UserService userService;

    //登录方法
    @PostMapping("/login")
    public String login(String adminName,String password){
        Admin loginAdmin = adminService.login(adminName,password);
        if(loginAdmin!=null){
            return "redirect:/adminPage";
        }else{
            return "redirect:/adminRegister";
        }
    }

    //注册方法
    @PostMapping("/register")
    public String register(Admin admin){
        admin.setId(UUIDUtils.getUUID());
        adminService.register(admin);
        return "redirect:/adminLogin";
    }

}
