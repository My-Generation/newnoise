package com.future.noise.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

    @GetMapping("/loginPage")
    public String toIndex(){
        return "web/login";
    }

    @GetMapping("/toRegister")
    public String toRegister(){
        return "web/register";
    }

    @GetMapping("/userPage")
    public String toUserPage(){
        return "web/userPage";
    }

    @GetMapping("/adminLogin")
    public String toAdminLogin(){
        return "web/adminLogin";
    }

    @GetMapping("/adminRegister")
    public String toAdminRegister(){
        return "web/adminRegister";
    }

    @GetMapping("/adminPage")
    public String toAdminPage(){
        return "web/adminPage";
    }

    @GetMapping("/saveUser")
    public String toSaveUser(){
        return "web/addUser";
    }

}
