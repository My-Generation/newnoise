package com.future.noise.service.impl;

import com.future.noise.dao.AdminDAO;
import com.future.noise.model.Admin;
import com.future.noise.service.AdminService;
import com.future.noise.utils.UUIDUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AdminServiceImpl implements AdminService {

    @Autowired
    private AdminDAO adminDAO;

    @Override
    public void register(Admin admin){
        adminDAO.save(admin);
    }

    @Override
    public Admin login(String adminName,String password){
        return adminDAO.queryByAdminNameAndPassword(adminName,password);
    }
}
